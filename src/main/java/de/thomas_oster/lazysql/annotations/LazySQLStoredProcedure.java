/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface LazySQLStoredProcedure {
    String value();
    String[] params() default "";
    boolean useObjectAsInput() default true;
    String methodName() default "";
    /**
     * If true, the db-connection will be opened with a try-with-resources block.
     * The Connection will be closed after the method
     * has finished.
     * If dependencyInjection is enabled,
     * the DataSource will be injected in the LazyDB
     * class, otherwise the DataSource will
     * be the first parameter instead of a connection.
     *
     */
    boolean manageConnectionWithDatasource() default false;

    /**
     * If true, the SQL Excetpion will be caught and a
     * runtime Exception will be thrown, so that the method
     * signature does not contain a checked exception.
     * @return
     */
    boolean convertSqlExceptionToRuntimeException() default false;
}
