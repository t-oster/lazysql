/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
public class UnknownRDBMSAdapter extends AbstractRDBMSAdapter {

    public UnknownRDBMSAdapter(String customDriverClass) throws DbConfig.ConfigException {
        if (!StringUtils.isEmpty(customDriverClass)) {
            tryToLoadDriver(customDriverClass);
        }
    }

    @Override
    public List<Argument> getSqlQueryType(Connection con, String sql, List<Argument> params) throws SQLException, NotSupportedException {
        throw new NotSupportedException("The RDBMS you are using is not yet supported by LazySQL. We cannot check the query result type, you have to provide the return type yourself");
    }

    @Override
    public void checkSQLSyntax(Connection con, String sql, List<Argument> params) throws SQLException, NotSupportedException {
        throw new NotSupportedException("The RDBMS you are using is not yet supported by LazySQL. We cannot check the query syntax.");
    }
    
}
