/*
 * The MIT License
 *
 * Copyright 2020 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import javax.annotation.processing.Messager;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

/**
 *
 * @author Thomas Oster (thomas.oster@upstart-it.de)
 */
public class DebugMessager implements Messager {
   private Messager m;
   public DebugMessager(Messager m) {
       this.m = m;
   }

    @Override
    public void printMessage(Diagnostic.Kind kind, CharSequence cs) {
        LazySQLProcessor.debugLog(kind.name()+": "+cs);
        m.printMessage(kind, cs);
    }

    @Override
    public void printMessage(Diagnostic.Kind kind, CharSequence cs, Element elmnt) {
        LazySQLProcessor.debugLog(kind.name()+": "+cs+" "+elmnt.toString());
        m.printMessage(kind, cs, elmnt);
    }

    @Override
    public void printMessage(Diagnostic.Kind kind, CharSequence cs, Element elmnt, AnnotationMirror am) {
        LazySQLProcessor.debugLog(kind.name()+": "+cs+" "+elmnt);
        m.printMessage(kind, cs, elmnt, am);
    }

    @Override
    public void printMessage(Diagnostic.Kind kind, CharSequence cs, Element elmnt, AnnotationMirror am, AnnotationValue av) {
        LazySQLProcessor.debugLog(kind.name()+": "+cs);
        m.printMessage(kind, cs, elmnt, am, av);
    }
}
