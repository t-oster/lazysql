/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import com.squareup.javapoet.TypeName;
import de.thomas_oster.lazysql.Argument.Direction;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Array;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Struct;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Data;
import org.apache.commons.lang.ClassUtils;

/**
 * A class representing support for an RDBMS. If your RDBMS need some special
 * treatment, extend this class and add your implementation to the getAdapterByDbUri
 * method
 * @author Thomas Oster (mail@thomas-oster.de)
 */
public abstract class AbstractRDBMSAdapter {
    
    //https://www.cs.mun.ca/java-api-1.5/guide/jdbc/getstart/mapping.html
    static final Map<Integer,Class> mappingNullable = new LinkedHashMap<>();
    static {
        mappingNullable.put(java.sql.Types.CHAR, String.class);
        mappingNullable.put(java.sql.Types.VARCHAR, String.class);
        mappingNullable.put(java.sql.Types.LONGVARCHAR, String.class);
        mappingNullable.put(java.sql.Types.LONGNVARCHAR, String.class);
        mappingNullable.put(java.sql.Types.NVARCHAR, String.class);
        mappingNullable.put(java.sql.Types.NCHAR, String.class);
        mappingNullable.put(java.sql.Types.NUMERIC, BigDecimal.class);
        mappingNullable.put(java.sql.Types.DECIMAL, BigDecimal.class);
        mappingNullable.put(java.sql.Types.BIT, Boolean.class);
        mappingNullable.put(java.sql.Types.TINYINT, Byte.class);
        mappingNullable.put(java.sql.Types.SMALLINT, Short.class);
        mappingNullable.put(java.sql.Types.INTEGER, Integer.class);
        mappingNullable.put(java.sql.Types.BIGINT, BigInteger.class);
        mappingNullable.put(java.sql.Types.REAL, Float.class);
        mappingNullable.put(java.sql.Types.FLOAT, Double.class);
        mappingNullable.put(java.sql.Types.DOUBLE, Double.class);
        mappingNullable.put(java.sql.Types.BINARY, byte[].class);
        mappingNullable.put(java.sql.Types.VARBINARY, byte[].class);
        mappingNullable.put(java.sql.Types.LONGVARBINARY, byte[].class);
        mappingNullable.put(java.sql.Types.DATE, Date.class);
        mappingNullable.put(java.sql.Types.TIME, Time.class);
        mappingNullable.put(java.sql.Types.TIMESTAMP, Timestamp.class);
        mappingNullable.put(java.sql.Types.CLOB, Clob.class);
        mappingNullable.put(java.sql.Types.ARRAY, Array.class);
        mappingNullable.put(java.sql.Types.STRUCT, Struct.class);
        mappingNullable.put(java.sql.Types.REF, Ref.class);
        mappingNullable.put(java.sql.Types.JAVA_OBJECT, Object.class);
    }
    
    public static class FullyQualifiedName {
        String catalog;
        String schema;
        String name;
    }
    
    protected final void tryToLoadDriver(String className) {
        try {
            Class<?> driver = Class.forName(className);
            DriverManager.registerDriver((Driver) driver.newInstance());
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(MssqlAdapter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    FullyQualifiedName getFqn(Connection con, String name) throws SQLException {
        FullyQualifiedName result = new FullyQualifiedName();
        String[] parts = name.split("[.]", -1);
        switch (parts.length) {
            case 3:
                result.catalog = parts[0];
                result.schema = parts[1];
                result.name = parts[2];
                break;
            case 2:
                result.catalog = con.getCatalog();
                result.schema = parts[0];
                result.name = parts[1];
                break;
            default:
                result.catalog = con.getCatalog();
                result.schema = con.getSchema();
                result.name = name;
                break;
        }
        return result;
    }
    
    public static class NotSupportedException extends Exception {
        public NotSupportedException(String reason) {
            super(reason);
            if (LazySQLProcessor.DEBUG_TO_FILE) {
                LazySQLProcessor.debugLog("NotSupportedException: "+reason);
            }
        }
    }
    
    public static AbstractRDBMSAdapter getAdapterByDbUri(String dburi, String customDriver) throws DbConfig.ConfigException {
        String driver = dburi.split(":")[1];
        if ("sqlserver".equals(driver)) {
            return new MssqlAdapter();
        }
        else if ("mysql".equals(driver)) {
            return new MysqlAdapter();
        }
        else if ("oracle".equals(driver)) {
            return new OracleAdapter();
        }
        else {
            return new UnknownRDBMSAdapter(customDriver);
        }
    }
    
    /**
     * Maps the SQL Column type to java class. Gets the information
     * from the getMetadata().getColumns method
     * if it returns null, the column is ignored.
     * @param DATA_TYPE the datatype from the metadata
     * @param TYPE_NAME the type name from the metadata
     * @param NULLABLE the nullable column from the metadata
     * @return the java Type corresponding to the SQL type name and nullable
     * information
     */
    protected Class getColumnType(int DATA_TYPE, String TYPE_NAME, int NULLABLE) {
        Class type = mappingNullable.get(DATA_TYPE);
        /**
         * We could use a primitve type for non-nullable values, but this
         * is ugly, because it does e.g. not work for Strings and also
         * this makes problems when using insert.
         */        
        return type;
    }
    
    public List<String> getSchemas(Connection con, String catalog, String prefix) throws SQLException {
        List<String> result = new LinkedList<>();
        ResultSet r = con.getMetaData().getSchemas(catalog, prefix != null ? prefix+"%" : null);
        while (r.next()) {
            result.add(r.getString("TABLE_SCHEM"));
        }
        return result;
    }
    
    public List<String> getSchemas(Connection con, String prefix) throws SQLException {
        return getSchemas(con, con.getCatalog(), prefix);
    }
    
    protected List<Argument> getInsertableTableColumns(Connection con, String catalog, String schema, String name, String prefix) throws SQLException {
        List<Argument> result = new LinkedList<>();
        ResultSet r = con.getMetaData().getColumns(catalog, schema, name, prefix != null ? prefix+"%" : null);
        while (r.next()) {
            boolean autoincrement = "YES".equals(r.getString("IS_AUTOINCREMENT"));
            boolean generated = false;
            try {
                generated = "YES".equals(r.getString("IS_GENERATEDCOLUMN"));
            }
            catch (SQLException e) {
                //some drivers do not support this column...
            }
            boolean optional = false;
            try {
                if (r.getString("COLUMN_DEF") != null && !"null".equals(r.getString("COLUMN_DEF"))) {
                    optional = true;
                }
            }
            catch (SQLException e) {
                //drivers do not support this column...
            }

            if (autoincrement || generated) {
                continue;
            }
            Class type = getColumnType(r.getInt("DATA_TYPE"), r.getString("TYPE_NAME"), r.getInt("NULLABLE"));
            if (type != null) {
                result.add(new Argument(r.getString("COLUMN_NAME"), type, optional));
            }
        }
        if (result.isEmpty()) {
            throw new SQLException("Unknown table `"+name+"`");
        }
        return result;
    }
    
    protected List<Argument> getTableColumns(Connection con, String catalog, String schema, String name, String prefix) throws SQLException {
        List<Argument> result = new LinkedList<>();
        ResultSet r = con.getMetaData().getColumns(catalog, schema, name, prefix != null ? prefix+"%" : null);
        while (r.next()) {
            Class type = getColumnType(r.getInt("DATA_TYPE"), r.getString("TYPE_NAME"), r.getInt("NULLABLE"));
            if (type != null) {
                result.add(new Argument(r.getString("COLUMN_NAME"), type, false));
            }
        }
        if (result.isEmpty()) {
            throw new SQLException("Unknown table `"+name+"`");
        }
        return result;
    }
    
    public List<Argument> getTableColumns(Connection con, String name, String prefix) throws SQLException {
        FullyQualifiedName fqn = getFqn(con, name);
        return getTableColumns(con, fqn.catalog, fqn.schema, fqn.name, prefix);
    }
    
    public List<Argument> getInsertableTableColumns(Connection con, String name, String prefix) throws SQLException {
        FullyQualifiedName fqn = getFqn(con, name);
        return getInsertableTableColumns(con, fqn.catalog, fqn.schema, fqn.name, prefix);
    }
    
    public List<Argument> getStoredProcedureParameters(Connection con, String catalog, String schema, String name) throws SQLException, NotSupportedException {
        List<Argument> result = new LinkedList<>();
        ResultSet r = con.getMetaData().getProcedureColumns(catalog, schema, name, null);
        while (r.next()) {
            if (r.getInt("COLUMN_TYPE") > 3) {
                //is result column
                continue;
            }
            Class type = mappingNullable.get(r.getInt("DATA_TYPE"));
            if (type == null) {
                throw new SQLException("Cannot map SQL Type "+r.getInt("DATA_TYPE"));
            }
            if (r.getInt("NULLABLE") == 0) {
                Class primitive = ClassUtils.wrapperToPrimitive(type);
                if (primitive != null) {
                    type = primitive;
                }
            }
            
            Direction dir = Direction.IN;
            if (r.getInt("COLUMN_TYPE") == 2) {
                dir = Direction.OUT;
            }
            result.add(new Argument(r.getString("COLUMN_NAME").replace("@", ""), TypeName.get(type), dir, false));
        }
        if (result.isEmpty()) {
            throw new SQLException("Unknown procedure `"+name+"`");
        }
        return result;
    }
    
    public List<Argument> getStoredProcedureParameters(Connection con, String name) throws SQLException, NotSupportedException {
        FullyQualifiedName fqn = getFqn(con, name);
        return getStoredProcedureParameters(con, fqn.catalog, fqn.schema, fqn.name);
    }
    
    @Data
    public static class Procedure {
        String name;
        String comment;
    }
    
    public List<Procedure> getStoredProcedures(Connection con, String catalog, String schema, String prefix) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        List<Procedure> result = new LinkedList<>();
        try (ResultSet r = con.getMetaData().getProcedures(catalog, schema, prefix+"%")) {
            while (r.next()) {
                Procedure p = new Procedure();
                p.name = r.getString("PROCEDURE_NAME");
                p.comment = r.getString("REMARKS");
                result.add(p);
            }
            r.close();
        }
        return result;
    }
    
    public List<Procedure> getStoredProcedures(Connection con, String prefix) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        FullyQualifiedName fqn = getFqn(con, prefix);
        return getStoredProcedures(con, fqn.catalog, fqn.schema, fqn.name);
    }
    
    /**
     * Let's the db analyze an SQL query and throws an exception or returns a
     * map containing the names and types of the resulting rows
     *
     * @param con the database connection
     * @param sql the sql query
     * @param params a list of parameters used in this query
     * @return returns a list of arguments corresponding to the query result type
     * @throws SQLException if the query is wrong
     * @throws de.thomas_oster.lazysql.AbstractRDBMSAdapter.NotSupportedException if the corresponding adapter cannot check the query result type
     */
    public abstract List<Argument> getSqlQueryType(Connection con, String sql, List<Argument> params) throws SQLException, NotSupportedException;

    public abstract void checkSQLSyntax(Connection con, String sql, List<Argument> params) throws SQLException, NotSupportedException;
    
    public List<String> getTables(Connection db, String catalog, String schema, String prefix) throws SQLException {
        List<String> result = new LinkedList<>();
        ResultSet r = db.getMetaData().getTables(catalog, schema, prefix == null ? null : prefix+"%", null);
        while (r.next()) {
            result.add(r.getString("TABLE_NAME"));
        }
        r.close();
        return result;
    };
    
    public List<String> getTables(Connection db, String prefix) throws SQLException {
        FullyQualifiedName fqn = getFqn(db, prefix);
        return getTables(db, fqn.catalog, fqn.schema, fqn.name);
    };
    
}
