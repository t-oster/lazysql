/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
public class MysqlAdapter extends AbstractRDBMSAdapter {

    public MysqlAdapter() {
        tryToLoadDriver("com.mysql.cj.jdbc.Driver");
    }

    @Override
    public List<Argument> getSqlQueryType(Connection con, String sql, List<Argument> params) throws SQLException, NotSupportedException {
        if (sql.contains(";")) {
            throw new NotSupportedException("LayzSQL cannot yet determine the result of this query");
        }
        //check if the query contains a limit in the outer most query
        String[] parts = sql.toLowerCase().split("\\)");
        String end = parts[parts.length-1];
        if (end.contains("limit")) {
            throw new NotSupportedException("LayzSQL cannot yet determine the result of this query");
        }
        List<Argument> a = new LinkedList<>();
        boolean ac = con.getAutoCommit();
        con.setAutoCommit(false);
        try {
            con.prepareStatement("DROP TEMPORARY TABLE IF EXISTS `temp`").execute();
            con.prepareStatement("CREATE TEMPORARY TABLE `temp` " +
                    sql + 
                " LIMIT 0; ").execute();
            try {
                PreparedStatement s = con.prepareStatement("DESCRIBE `temp`");
                s.execute();
                ResultSet r = s.getResultSet();
                while (r.next()) {
                    a.add(new Argument(
                        r.getString("Field"),
                        toJavaType((r.getString("Type")))
                    ));
                }
            }
            finally {
                con.prepareStatement("DROP TEMPORARY TABLE IF EXISTS `temp`").execute();
            }
        }
        finally {
            con.rollback();
            con.setAutoCommit(ac);
        }
        return a;
    }
    
    //TODO https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-reference-type-conversions.html
    private Type toJavaType(String sqlType) throws SQLException {
        if (sqlType.startsWith("varchar") || sqlType.startsWith("nvarchar") || sqlType.endsWith("text") || sqlType.startsWith("json")) {
            return String.class;
        } else if (sqlType.startsWith("double")) {
            return Double.class;
        } else if (sqlType.startsWith("float")) {
            return Float.class;
        } else if (sqlType.endsWith("int")) {
            return Integer.class;
        } else if (sqlType.startsWith("decimal") || sqlType.startsWith("numeric")) {
            return BigDecimal.class;
        } else if (sqlType.equals("xml")) {
            return String.class;
        } else if (sqlType.equals("uniqueidentifier")) {
            return String.class;
        } else if (sqlType.equals("date")) {
            return Date.class;
        } else if (sqlType.equals("timestamp") || sqlType.equals("datetime")) {
            return Timestamp.class;
        } else if (sqlType.startsWith("char")) {
            return String.class;
        } else if (sqlType.equals("bit")) {
            return Boolean.class;
        }
        throw new SQLException("Unknown Type " + sqlType);
    }

    @Override
    public void checkSQLSyntax(Connection con, String sql, List<Argument> params) throws SQLException, NotSupportedException {
        throw new NotSupportedException("MySQL Adapter cannot yet validate syntax");
    }
    
}
