/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import com.squareup.javapoet.TypeName;
import static de.thomas_oster.lazysql.TypeHelper.getSimpleName;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
public class MssqlAdapter extends AbstractRDBMSAdapter {

    public MssqlAdapter() {
        tryToLoadDriver("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    }

    @Override
    public List<Procedure> getStoredProcedures(Connection con, String catalog, String schema, String prefix) throws SQLException, NotSupportedException {
        List<Procedure> result = super.getStoredProcedures(con, catalog, schema, prefix);
        //fix: the MSSQL driver seems to append ";<number>" to the name
        result.forEach(p -> p.setName(p.getName().split(";")[0]));
        return result;
    }

    @Override
    protected Class getColumnType(int DATA_TYPE, String TYPE_NAME, int NULLABLE) {
        if ("timestamp".equals(TYPE_NAME)) {
            //Timestamp is the internal rowversion type which cannot be written
            //so we ignore it
            return null;
        }
        return super.getColumnType(DATA_TYPE, TYPE_NAME, NULLABLE);
    }
    
    @Override
    public List<Argument> getSqlQueryType(Connection con, String sql, List<Argument> params) throws SQLException {
        Statement stmtObj = con.createStatement();
        String paramsSQL = "";
        if (params != null) {
            paramsSQL = params
                    .stream()
                    .map(p -> '@'+p.getName()+' '+getSQLType(p.getType()))
                    .collect(Collectors.joining(",", ", @params = N'", "'"));
        }
        ResultSet resObj = stmtObj.executeQuery("exec sp_describe_first_result_set @tsql = N'" + sql.replaceAll("'", "''") + "' "+paramsSQL);
        List<Argument> result = new LinkedList<>();
        while (resObj.next()) {
            result.add(new Argument(
                    resObj.getString("name"),
                    toJavaType(resObj.getString("system_type_name"))
            ));
        }
        return result;
    }
    
    //TODO: http://doraprojects.net/blog/?p=2300
    private String getSQLType(TypeName t) {
        switch (getSimpleName(t)) {
            case "String":
                return "VARCHAR";
            case "Integer":
            case "int":
                return "INT";
            case "Date":
                return "DATETIME";
            default:
                return "VARCHAR";
        }
    }

    //TODO http://doraprojects.net/blog/?p=2300
    private Type toJavaType(String sqlType) throws SQLException {
        if (sqlType.startsWith("varchar") || sqlType.startsWith("nvarchar")) {
            return String.class;
        } else if (sqlType.equals("int")) {
            return Integer.class;
        } else if (sqlType.equals("tinyint")) {
            return Byte.class;
        } else if (sqlType.startsWith("decimal") || sqlType.startsWith("numeric")) {
            return BigDecimal.class;
        } else if (sqlType.startsWith("float")) {
            return Double.class;
        } else if (sqlType.equals("xml")) {
            return String.class;
        } else if (sqlType.equals("uniqueidentifier")) {
            return String.class;
        } else if (sqlType.equals("datetime") || sqlType.equals("date")) {
            return Date.class;
        } else if (sqlType.equals("timestamp") || sqlType.equals("image")) {
            return byte[].class;
        } else if (sqlType.equals("char")) {
            return String.class;
        } else if (sqlType.startsWith("char(") || sqlType.startsWith("text")) {
            return String.class;
        } else if (sqlType.equals("bit")) {
            return Boolean.class;
        } else if (sqlType.startsWith("bigint")) {
            return Long.class;
        }
        throw new SQLException("Unknown Type " + sqlType);
    }

    @Override
    public void checkSQLSyntax(Connection con, String sql, List<Argument> params) throws SQLException {
        try {
            getSqlQueryType(con, sql, params);
        }
        catch (SQLException ex) {
            if (ex.getErrorCode() != 11526) {
                //11526 =cannot determine result type but query seems OK and
                throw (ex);
            }
        }
    }
    
}
