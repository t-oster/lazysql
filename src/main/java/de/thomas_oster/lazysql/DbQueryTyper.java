/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import de.thomas_oster.lazysql.AbstractRDBMSAdapter.Procedure;
import java.io.File;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.annotation.processing.Messager;
import javax.tools.Diagnostic.Kind;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
public class DbQueryTyper {

    private static class DbCon {
        public Lazy<Connection,AbstractRDBMSAdapter.NotSupportedException> con;
        public ArgumentListCache cache;
        public AbstractRDBMSAdapter rdbms;
    }
    
    private static final Map<DbConfig, DbCon> cache = new LinkedHashMap<>();
    
    private Lazy<Connection,AbstractRDBMSAdapter.NotSupportedException> db;
    private ArgumentListCache fileCache;
    private AbstractRDBMSAdapter rdbms;

    DbQueryTyper(DbConfig cfg, Messager messager) throws SQLException, DbConfig.ConfigException {
        if (cfg != null) {
            if (cache.containsKey(cfg)) {
                db = cache.get(cfg).con;
                fileCache = cache.get(cfg).cache;
                rdbms = cache.get(cfg).rdbms;
            }
            else {
               DriverManager.setLoginTimeout(5);
                final Properties properties = new Properties();
                properties.put("connectTimeout", "2000");
                properties.put("user", cfg.dbuser);
                properties.put("password", cfg.dbpassword);
                final String dburl = cfg.dburl;
                db = new Lazy<>(() -> 
                    {
                        try {
                            if (messager != null) {
                                messager.printMessage(Kind.NOTE, "Connecting to DB "+cfg.dburl);
                                    }
                            return DriverManager.getConnection(dburl, properties);
                                    }
                        catch (SQLException ex) {
                            throw new AbstractRDBMSAdapter.NotSupportedException(ex.getLocalizedMessage());
                        }
                    }
                );
                fileCache = new ArgumentListCache(StringUtils.isEmpty(cfg.cachefile) ? null : new File(cfg.cachefile));
                rdbms = AbstractRDBMSAdapter.getAdapterByDbUri(dburl, cfg.customDriver);
                DbCon c = new DbCon();
                c.cache = fileCache;
                c.con = db;
                c.rdbms = rdbms;
                cache.put(cfg, c);
                
            }
            
        }
        if (db == null) {
            //still possible to compile...
            throw new SQLException("No Config found");
        }

    }

    public List<String> getTables(String prefix) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return fileCache.getStrings("GET-TABLES "+prefix, 
            () -> rdbms.getTables(db.get(), prefix)
        );
    }
    
    public List<String> getTables(String catalog, String schema, String prefix) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return fileCache.getStrings("GET-TABLES "+catalog+"#"+schema+"#"+prefix, 
            () -> rdbms.getTables(db.get(), catalog, schema, prefix)
        );
    }
    
    public List<String> getSchemas(String prefix) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return fileCache.getStrings("GET-SCHEMAS "+prefix, 
            () -> rdbms.getSchemas(db.get(), prefix)
        );
    }
    
    public AbstractRDBMSAdapter.FullyQualifiedName getFqn(String name) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return rdbms.getFqn(db.get(), name);
    }
    
    public List<Argument> getTableColumns(String name, String prefix) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return fileCache.get(name+"#"+prefix, () -> {
            return rdbms.getTableColumns(db.get(), name, null);
        });
    }
    
    public List<Argument> getInsertableTableColumns(String name, String prefix) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return fileCache.get("insertable#"+name+"#"+prefix, () -> {
            return rdbms.getInsertableTableColumns(db.get(), name, null);
        });
    }
    
    public List<Procedure> getStoredProcedures(String catalog, String schema, String prefix) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return fileCache.getProcedures("GET-PROCEDURES "+catalog+"#"+schema+"#"+prefix, 
            () -> rdbms.getStoredProcedures(db.get(), catalog, schema, prefix)
        );
    }
    
    public List<Procedure> getStoredProcedures(String prefix) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return fileCache.getProcedures("GET-PROCEDURES "+prefix, 
            () -> rdbms.getStoredProcedures(db.get(), prefix)
        );
    }
    
    public List<Argument> getStoredProcedureParameters(String name) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return fileCache.get(name, () -> {
            return rdbms.getStoredProcedureParameters(db.get(), name);
        });
    }

    /**
     * Let's the db analyze an SQL query and throws an exception or returns a
     * map containing the names and types of the resulting rows
     *
     * @param sql the sql query
     * @param params a list of parameters used in this query
     * @return returns a list of arguments corresponding to the query result type
     * @throws SQLException if the query is invalid
     * @throws de.thomas_oster.lazysql.AbstractRDBMSAdapter.NotSupportedException if the corresponding adapter cannot check the query result type
     */
    public List<Argument> getSqlQueryType(String sql, List<Argument> params) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        return fileCache.get(sql+"#WITH#"+Argument.toArgumentList(params), () -> {
            if (LazySQLProcessor.DEBUG_TO_FILE) {
                LazySQLProcessor.debugLog("not in cache. Asking DB...");
                List<Argument> result = rdbms.getSqlQueryType(db.get(), sql, params);
                LazySQLProcessor.debugLog("Result: "+Argument.toTypeListString(result));
                return result;
            }
            return rdbms.getSqlQueryType(db.get(), sql, params);
        });
    }
    
    void checkSQLSyntax(String sql, List<Argument> params) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        fileCache.get(sql+"#WITH#"+Argument.toArgumentList(params), () -> {
            rdbms.checkSQLSyntax(db.get(), sql, params);
            return new LinkedList<>();
        });
    }
}
