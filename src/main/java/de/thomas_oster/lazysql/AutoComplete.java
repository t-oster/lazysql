/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import de.thomas_oster.lazysql.AbstractRDBMSAdapter.Procedure;
import de.thomas_oster.lazysql.annotations.LazySQLInsert;
import de.thomas_oster.lazysql.annotations.LazySQLSelect;
import de.thomas_oster.lazysql.annotations.LazySQLStoredProcedure;
import de.thomas_oster.lazysql.annotations.LazySQLUpsert;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.processing.Completion;
import javax.annotation.processing.Completions;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
public class AutoComplete {
    
    private static String getNonemptyMemberValue(AnnotationMirror annotation, String property) throws Exception {
        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> v : annotation.getElementValues().entrySet()) {
            if (v.getKey().toString().startsWith(property)) {
                String result = v.getValue().toString();
                if (!StringUtils.isEmpty(result)) {
                    return result;
                }
                break;
            }
        }
        throw new Exception("Member not found or empty");
    }
    
    public static Iterable<? extends Completion> getCompletions(LazySQLProcessor p, Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        List<Completion> result = new LinkedList<>();
        try {
            if (LazySQLSelect.class.getName().equals(annotation.getAnnotationType().toString())) {
                if ("returns".equals(member.getSimpleName().toString())) {
                    String query = getNonemptyMemberValue(annotation, "value")
                        .replaceAll("\\{\"", "")
                        .replaceAll("\"\\}", "")
                        .replaceAll("\", \"", " ")
                        .replaceAll("\\\\'", "'")
                        .replace("{WHERE}", "WHERE 1 = 1")
                        .replaceAll(":", "@");
                    DbQueryTyper db = p.getDatabaseForElement(element, null, null);
                    String returnTypesAsString = Argument.toArgumentList(db.getSqlQueryType(query, null));
                    result.add(Completions.of("\""+returnTypesAsString+"\"", "Returns Type from db"));
                }
                if ("value".equals(member.getSimpleName().toString())) {
                    String[] current = userText.toLowerCase().replaceAll("\"", "").split(" ");
                    if (current.length > 2) {
                        String plast = current[current.length-2];
                        String last = current[current.length-1];
                        if ("from".equals(last) || "join".equals(last)) {
                            plast = last;
                            last = "";
                        }
                        if ("from".equals(plast) || "join".equals(plast)) {
                            DbQueryTyper db = p.getDatabaseForElement(element, null, null);
                            for (String t : db.getTables(last)) {
                                String ut = userText.replaceAll("\"", "");
                                result.add(Completions.of("\""+ut+t.substring(last.length())+"\"", "Tables from db"));
                            }
                        }
                    }
                    
                }
            }
            if (LazySQLStoredProcedure.class.getName().equals(annotation.getAnnotationType().toString())) {
                if ("params".equals(member.getSimpleName().toString())) {
                    String procedure = getNonemptyMemberValue(annotation, "value").replaceAll("\"", "");
                    DbQueryTyper db = p.getDatabaseForElement(element, null, null);
                    String returnTypesAsString = Argument.toArgumentList(db.getStoredProcedureParameters(procedure));
                    result.add(Completions.of("\""+returnTypesAsString+"\"", "Params from db"));
                }
                if ("value".equals(member.getSimpleName().toString())) {
                    DbQueryTyper db = p.getDatabaseForElement(element, null, null);
                    String text = userText.replaceAll("\"", "");
                    if (!text.contains(".")) {
                        for (Procedure sp: db.getStoredProcedures(text)) {
                            result.add(Completions.of("\""+sp.getName()+"\"", ""+sp.getComment()));
                        }
                        for (String schema: db.getSchemas(text)) {
                            result.add(Completions.of("\""+schema+".", "Schema "));
                        }
                    }
                    else {
                        AbstractRDBMSAdapter.FullyQualifiedName fqn = db.getFqn(text);
                        for (Procedure sp: db.getStoredProcedures(fqn.catalog, fqn.schema, fqn.name)) {
                            result.add(Completions.of("\""+fqn.schema+"."+sp.getName()+"\"", ""+sp.getComment()));
                        }
                    }
                    
                }
            }
            if (LazySQLInsert.class.getName().equals(annotation.getAnnotationType().toString())
                || LazySQLUpsert.class.getName().equals(annotation.getAnnotationType().toString())) {
                if ("params".equals(member.getSimpleName().toString())) {
                    String table = getNonemptyMemberValue(annotation, 
                        LazySQLInsert.class.getName().equals(annotation.getAnnotationType().toString()) ? "value" : "table")
                        .replaceAll("\"", "");
                    DbQueryTyper db = p.getDatabaseForElement(element, null, null);
                    String returnTypesAsString = Argument.toArgumentList(db.getTableColumns(table, null));
                    result.add(Completions.of("\""+returnTypesAsString+"\"", "Table Columns from db"));
                }
                if ("value".equals(member.getSimpleName().toString()) || "table".equals(member.getSimpleName().toString())) {
                    DbQueryTyper db = p.getDatabaseForElement(element, null, null);
                    userText = userText.replaceAll("\"", "");
                    if (!userText.contains(".")) {
                        for (String t : db.getTables(userText)) {
                            result.add(Completions.of("\""+t+"\"", "Tables from db"));
                        }
                        for (String schema: db.getSchemas(userText)) {
                            result.add(Completions.of("\""+schema+".", "Schema "));
                        }
                    }
                    else {
                        AbstractRDBMSAdapter.FullyQualifiedName fqn = db.getFqn(userText);
                        for (String t : db.getTables(fqn.catalog, fqn.schema, fqn.name)) {
                            result.add(Completions.of("\""+fqn.schema+"."+t+"\"", "Tables from db"));
                        }
                    }
                    
                }
            }
            if (LazySQLUpsert.class.getName().equals(annotation.getAnnotationType().toString())) {
                if ("keys".equals(member.getSimpleName().toString())) {
                    String table = getNonemptyMemberValue(annotation, "table")
                        .replaceAll("\"", "");
                    DbQueryTyper db = p.getDatabaseForElement(element, null, null);
                    String text = userText.replaceAll("\"", "");
                    String[] keys = text.split(",");
                    String last = keys[keys.length-1].trim();
                    for (Argument column : db.getTableColumns(table, last)) {
                        //bug: should have been filtered
                        if (!column.getName().toLowerCase().startsWith(last.toLowerCase())) {
                            continue;
                        }
                        result.add(Completions.of("\""+text+column.getName().replace(last, "")+"\"", "Columns from DB"));
                    }
                }
            }
        }
        catch (Exception e) {
            //LazySQLProcessor.debugLog("Ex2: "+e);
        } 
//        String fqn = ((TypeElement) annotation.getAnnotationType().asElement()).getQualifiedName().toString();
//        if (db != null && fqn.equals(LazySQLSelect.class.getCanonicalName()) && userText.toLowerCase().contains("from")) {
//            String[] parts = userText.split("from", 2);
//            if (!parts[1].trim().contains(" ")) {
//                try {
//                    String prefix = parts[1].trim();
//                    String space = parts[1].charAt(0) == ' ' ? " " : "";
//                    return db.getTables(prefix).stream().map(name -> {return Completions.of(parts[0]+" from "+space+name);}).collect(Collectors.toList());
//                } catch (SQLException ex) {
//                    
//                }
//            }
//        }
        return result;
    }
}
