/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import com.squareup.javapoet.TypeName;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
public class OracleAdapter extends AbstractRDBMSAdapter {

    public OracleAdapter() {
        tryToLoadDriver("oracle.jdbc.driver.OracleDriver");
    }
    
    @Override
    public List<Argument> getTableColumns(Connection con, String name, String prefix) throws SQLException {
        return getTableColumns(con, con.getCatalog(), null, name, prefix);
    }

    @Override
    public List<Procedure> getStoredProcedures(Connection con, String prefix) throws SQLException, NotSupportedException {
        return super.getStoredProcedures(con, con.getCatalog(), null, prefix);
    }

    @Override
    public List<Argument> getStoredProcedureParameters(Connection con, String name) throws SQLException, NotSupportedException {
        return super.getStoredProcedureParameters(con, con.getCatalog(), null, name);
    }
    
    @Override
    public List<Argument> getSqlQueryType(Connection con, String sql, List<Argument> params) throws SQLException, NotSupportedException {
        if (sql.toLowerCase().replaceAll("\n", " ").contains(" rownum ") || sql.contains(";")) {
            throw new NotSupportedException("LayzSQL cannot yet determine the result of this query");
        }
        if (params != null) {
            for (Argument p : params) {
                sql = sql.replaceAll("@"+p.getName(), p.getType().equals(TypeName.get(String.class)) ? "''" : "0");
            }
        }
        else {
            while (sql.contains("@")) {
            String name = sql.split("@")[1].split("[ )(,]")[0];
                sql = sql.replaceAll("@" + name, "''");
            }
        }
        List<Argument> a = new LinkedList<>();
        boolean ac = con.getAutoCommit();
        con.setAutoCommit(false);
        try {
            PreparedStatement s = con.prepareStatement("CREATE TABLE UIT_TMP_VIEW AS " +
                    sql 
            + (sql.toLowerCase().contains("where") ? " AND " : " WHERE ")+"ROWNUM = 0");
            s.execute();
            try {
                a = getTableColumns(con, "UIT_TMP_VIEW", null);
            }
            finally {
                con.prepareStatement("DROP TABLE UIT_TMP_VIEW").execute();
            }
        }
        finally {
            con.rollback();
            con.setAutoCommit(ac);
        }
        return a;
    }
    
    @Override
    public void checkSQLSyntax(Connection con, String sql, List<Argument> params) throws SQLException, NotSupportedException {
        throw new NotSupportedException("Not supported yet.");
    }

}
