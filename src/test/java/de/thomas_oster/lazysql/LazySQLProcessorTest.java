/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import lombok.Data;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Thomas Oster <thomas.oster@upstart-it.de>
 */
public class LazySQLProcessorTest {
    
    private static @Data class Message {
        final Diagnostic.Kind kind;
        final CharSequence cs;
    }
    /**
     * contains the last message which was written from our dummy instance
     */
    private Message lastMessage;
    
    /**
     * Instance of SQL Processor with Dummy Messager
     */
    Lazy<LazySQLProcessor, RuntimeException> instance = Lazy.of(() -> {
        LazySQLProcessor p = new LazySQLProcessor();
        p.init(new ProcessingEnvironment() {
            @Override
            public Map<String, String> getOptions() { return null; }
            @Override
            public Messager getMessager() { return new Messager() {
                @Override
                public void printMessage(Diagnostic.Kind kind, CharSequence cs) {
                    lastMessage = new Message(kind, cs);
                }

                @Override
                public void printMessage(Diagnostic.Kind kind, CharSequence cs, Element elmnt) {
                    lastMessage = new Message(kind, cs);
                }

                @Override
                public void printMessage(Diagnostic.Kind kind, CharSequence cs, Element elmnt, AnnotationMirror am) {
                    lastMessage = new Message(kind, cs);
                }

                @Override
                public void printMessage(Diagnostic.Kind kind, CharSequence cs, Element elmnt, AnnotationMirror am, AnnotationValue av) {
                    lastMessage = new Message(kind, cs);
                }
            }; }
            @Override
            public Filer getFiler() { return null; }
            @Override
            public Elements getElementUtils() { return null; }
            @Override
            public Types getTypeUtils() { return null; }
            @Override
            public SourceVersion getSourceVersion() { return null; }
            @Override
            public Locale getLocale() { return null; }
        });
        return p;
    });
    
    @Test
    public void testCheckIfAllArgumentsAreUsed() throws Exception {
        //should be OK
        instance.get().checkIfAllArgumentsAreUsed(
            "SELECT bla WHERE test = :blubb AND Date like '%d:%m %h:%y",
            Argument.fromTypeListString("String blubb"),
            null
        );
        //should warn about unused bla
        instance.get().checkIfAllArgumentsAreUsed(
            "SELECT bla WHERE test1 = :blubb AND Date like '%d:%m %h:%y",
            Argument.fromTypeListString("String blubb, String bla"),
            null
        );
        assertEquals("Der Methodenparameter bla wird nicht in der SQL Query verwendet", lastMessage.cs);
        //should throw exception because of non declaed bla
        try {
            instance.get().checkIfAllArgumentsAreUsed(
                "SELECT bla WHERE test1 = :blubb AND test2 = :bla Date like '%d:%m %h:%y",
                Argument.fromTypeListString("String blubb"),
                null
            );
            fail("Should throw Exception");
        }
        catch (Exception e) {
            assertEquals("Die SQL Variable :bla existiert nicht als Parameter", e.getMessage());
        }
        //should be OK with same prefixes
        lastMessage = null;
        instance.get().checkIfAllArgumentsAreUsed(
            "SELECT bla WHERE test = :bla AND test2 = :blabl Date like '%d:%m %h:%y",
            Argument.fromTypeListString("String bla, String blabl"),
            null
        );
        assertNull(lastMessage);
    }
    
    @Test
    public void testReplaceArgumentsAndListOrder() throws Argument.MismatchException, Argument.ParsingException {
        List<Argument> result = new LinkedList<>();
        String qresult = instance.get().replaceArgumentsAndListOrder(
            "SELECT :bla AND :blubb AND :bla AND :blubber AND date = '%h:%i'",
            Argument.fromTypeListString("String blubber, int bla, Double blubb"),
            result);
        assertEquals("SELECT ? AND ? AND ? AND ? AND date = '%h:%i'", qresult);
        assertEquals(Argument.fromTypeListString("int bla, Double blubb, int bla, String blubber"), result);
    }

}
