#!/bin/bash
set -e
export JAVA_HOME=/lib/jvm/java-17-openjdk
mvn -P release clean deploy