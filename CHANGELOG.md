# Changelog

## 1.19.1-SNAPSHOT (in progress)
* TODO support stored procedures with return value
* TODO if two methods use the same return class, check if they are identical
* TODO support stored procedures with custom parameter type 

## 1.19.0
* Support optional arguments in insert
* Support return of the primary key in insert
* Support insert without any parameters

## 1.18
* Add convertSqlExceptionToRuntimeException and manageConnectionWithDatasource to all Annotations
* Fix escaping
* Make constructors public

## 1.17.9
* Fix: Fix: if returnTypeClass is not specified, and exactly one additionalInterface is specified, use this as return type

## 1.17.8
* Fix: if returnTypeClass is not specified, and exactly one additionalInterface is specified, use this as return type

## 1.17.7
* Fix: If only returnTypeInterface is specified, use the interface as return type

## 1.17.6
* bugfix

## 1.17.5
* additionalInterfacesC of type class[], additionalInterfaces of type string[] because of circular dependency issues

## 1.17.4
* additionalInterfaces of type class[]

## 1.17.3
* Add Support for manageConnectionWithDatasource and convertSqlExceptionToRuntimeException for @LazySQLSelect

## 1.17.1
* Add Support for excludeColumns in @LazySQLSelect

## 1.17.0
* Add Support for returnClassName and additionalInterfaces in @LazySQLSelect

## 1.16.0
* Mirgrate to Java 17

## 1.15.1
* Fix Type detection for MySQL Text and (Big,Small,Medium,Tiny-) Int

## 1.15
* Remove newlines in Queries
* Fix char(n) for Mssql

## 1.14
* Big Refactoring: Don't wrap the java.sql.Connection in the Class, use it as argument for each method,
so the Caller is responsible for opening/closing. Wrap all preparted Statements in a try-with-resources block
so they get closed propery. This breaks compatibility to previous versions!

## 1.13
* Fix: Some drivers do not support IS_GENERATEDCOLUMN, which broke Upsert
* Fix: Some dependencies were missing when running with Java 11, which caused NetBeans CompileOnSave to not work

## 1.12
* Fix: ignore auto-increment and generated columns for insert and upsert
* Fix: include jilt library in jar (shaded) so no dependency to jcenter is needed

## 1.11
* Allow multiple Strings in most annotations (for params and returns), Join with ','
  without breaking compatibility
* Support reading Spring Boot's application.yml for configuration
* Upsert supports `onlyUpdate` parameter in order to only update a few fields instead of all non-keys
* Fix: Allow writing null parameters for Timestamp and BigInteger columns
* Do not write ERRORS, but MANDATORY_WARNINGs so the code still get's compiled an error
  in one file does not break the whole projects. Since we do not generate methods
  with errors in the annotations, this will still result in a compiler error
  if the generated code is used, so this makes more sense (even if it is only shown
  as yellow warning instead of red error in netbeans

## 1.10
* Interpret Mysql datatype `json` as String
* Do not use primitives to indicate non-nullability. It is not clean because it
  only works for numeric types and booleans and makes problems with insert etc
* Do always capitalize getters/setters (otherwise we have problems with JSF). But for
  proper serialization/deserialization add the possibility to add Jackson Annotations

## 1.9
* better caching (cache table names, procedure names etc)
* some fixes regarding to getter/setter names
* Fix SQL Parameter detection for not detecting stuff like %h:%i

## 1.8 First version on maven central
